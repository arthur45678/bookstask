<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/



Route::group([

   // 'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::middleware(['api'])->group(function () {
        Route::post('login',[\App\Http\Controllers\AuthController::class,'login']);
        Route::post('signup',[\App\Http\Controllers\AuthController::class,'signup']);
        Route::post('logout',[\App\Http\Controllers\AuthController::class,'logout']);
        Route::post('refresh',[\App\Http\Controllers\AuthController::class,'refresh']);
        Route::post('me',[\App\Http\Controllers\AuthController::class,'me']);
    });



  /*  Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');*/

});


Route::apiResource('/employee', \App\Http\Controllers\Api\EmployeeController::class);
Route::apiResource('/books', \App\Http\Controllers\Api\BooksController::class);
