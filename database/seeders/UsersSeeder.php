<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Models\User::insert([
            [
                'name' => 'Arthur',
                'password' => bcrypt('password'),
                'email' => 'arthur@example.com',

            ],
            [
                'name' => 'Saqulik',
                'password' => bcrypt('password'),
                'email' => 'saqulik@example.com',
            ],

        ]);
    }
}
