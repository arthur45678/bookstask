<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Book extends Model
{
    use HasFactory;

    protected $fillable = ['genre','author','title','year',];


    public static function storeBook(Request $request)
    {
        $item = new self();
        $data = $request->only($item->fillable);
        return self::create($data);
    }

    public static function updateBook(Request $request, $id)
    {
        $item = new self();
        $data = $request->only($item->fillable);
        DB::table('books')->where('id',$id)->update($data);
    }


}
