
// Books
let storeBook = require('./components/books/create.vue').default;
let books = require('./components/books/index.vue').default;
let editBook = require('./components/books/edit.vue').default;

export const routes = [
    // Book
    { path: '/', component: books, name:'books'},
    { path: '/store-book', component: storeBook, name:'store-book'},
    { path: '/books', component: books, name:'books'},
    { path: '/edit-book/:id', component: editBook, name:'edit-book'},

]
